package com.example.springbootprofiles;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ProfileConfiguration {

	@Profile("dev")
	@Bean
	public String devEnvironment() {
		System.out.println("DEV-environment");
		return "DEV-environment";
	}

	@Profile("prod")
	@Bean
	public String prodEnvironment() {
		System.out.println("PROD-environment");
		return "PROD-environment";
	}

	@Profile("test")
	@Bean
	public String testEnvironment() {
		System.out.println("TEST-environment");
		return "TEST-environment";
	}
}
